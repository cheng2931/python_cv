# 1.Dlib简介 

**Dlib是基于c++写的库**

![cv1](https://gitee.com/cheng2931/python_cv/raw/master/cvimg/cv1.png)

**项目地址**：https://github.com/ageitgey/face_recognition/

**中文文档**：https://github.com/ageitgey/face_recognition/blob/master/README_Simplified_Chinese.md

# 2.安装face_recognition

Windows系统安装face_recognition：

**要求：**

1. 安装了 C/C++ Compiler 的 Microsoft Visual Studio 2015（或更高版本）

   ![cv2](https://gitee.com/cheng2931/python_cv/raw/master/cvimg/cv2.png)

2. python3  64位

3. [`CMake`](https://cmake.org/download/) for Windows 并将其添加到系统环境变量中。

**过程：**

1. pip install cmake
2. pip install dlib
3. pip install face_recognition





