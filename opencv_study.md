# 1.图片读取和存储

## 1.读取图像

**语法：**cv2.imread(param1,param2)     

参数1：图片的路径 

参数2：

0：以灰度模式读入图像

1：读入一幅彩色图像，图像的透明度会被忽略(默认为1)

-1：读入一幅图像，包括图像的alpha通道

```python
# -*- coding: utf-8 -*-
import cv2

#读取图片
frame=cv2.imread('imgs/3.jpg',0)  #灰色图像
frame1=cv2.imread('imgs/3.jpg',1)  #彩色图像
print(type(frame1),frame1.shape)
# <class 'numpy.ndarray'>   (773, 580, 3)
```

## 2.显示图像

**方法一**：cv2.imshow()

功能：使用窗口显示函数，窗口大小自动适应图片大小

```python
# 显示图片
cv2.imshow('yan',frame1) #创建显示窗口，窗口名，图像
# 键盘绑定函数
cv2.waitKey(0)  # 0表示等待键盘输入，按任意键继续运行程序
# 销毁所有资源
cv2.destroyAllWindows()  # 删除窗口

```

**方法二**:cv2.namedWindow()先创建窗口

功能：先创建窗口，后显示图像

参数：

cv2.WINDOW_AUTOSIZE：自动生成窗口大小 (默认)

cv2.WINDOW_NORMAL：允许调整窗口大小

```python
cv2.namedWindow('img',cv2.WINDOW_AUTOSIZE)
cv2.imshow('img',frame1)
cv2.waitKey(0)
cv2.destroyAllWindows()
```

**方法三**：用matplotlib库

```python
import cv2
import matplotlib.pyplot as plt
frame1=cv2.imread('imgs/3.jpg',1)  #彩色图像
plt.imshow(frame1)
plt.show()
```

**这种方法颜色会出现问题，原因是cv2.imread读出来的图像是GBR的顺序存储，**

**而matplotlib是RGB的顺序，所以需要用cv2.cvtColor函数转换一下**

```python
frame1=cv2.imread('imgs/3.jpg')  #彩色图像

img=cv2.cvtColor(frame1,cv2.COLOR_BGR2RGB)
plt.imshow(img)
plt.show()
```

## 3.保存图像

使用cv2.imwrite()来保存图像

首先需要一个文件名，然后是你要保存的图像

```python
# 保存图像
cv2.imwrite('imgs/test.jpg',frame)
# 按键盘上的q键保存图像
if k == ord('s'):
    cv2.imwrite('imgs/test.jpg',frame)
```



# 2.画图函数

## 1.画线

要告诉函数这条线的起点和终点

## 2.画方形

参数：左上角坐标和右下角坐标

## 3.画圆

只需指定圆的中心坐标和半径大小

## 4.画椭圆

cv2.ellipse(img,center,axes,angle,startAngle,endAngle,color)

img:画椭圆的图片

center:椭圆中心点的坐标

axes:长轴和短轴的长度

angle:整个椭圆沿顺时针方向旋转的角度

startAngle，endAngle:椭圆弧面沿顺时针方向起始的角度和结束的角度，如果是0，360就是整个椭圆，180就是半个椭圆

## 5.画多边形

实际实画一组线段

绘制多边形，首先需要顶点坐标

将这些点组成ROWSx1x2的数组，其中ROWS是顶点的数量，类型必须是int32

## 6.图片添加文字

参数：

- 绘制的文字
- 绘制的位置
- 字体类型
- 字体大小
- 文字的一般属性如颜色，粗细，线条的类型等

```python
# -*- coding: utf-8 -*-
import cv2
import numpy as np
#生成一张黑色图像
img=np.zeros((512,512,3),np.uint8)
# 绘画线条
cv2.line(img,(0,0),(511,510),(165,255,345),10)

"""最后一个参数-1表示全填充，其他表示线的粗细"""

# 绘画长方形
cv2.rectangle(img,(111,111),(222,222),(255,123,111),3)
# 圆形
cv2.circle(img,(333,333),100,(111,55,333),6)
# 椭圆
cv2.ellipse(img,(256,256),(100,50),0,0,360,(21,33,55),-1) 

# 多边形
pts=np.array([[100,100],[300,150],[400,400],[50,350]],np.int32)
print(np.shape(pts))
# pts=pts.reshape((-1,1,2))
# print(np.shape(pts))
cv2.polylines(img,[pts],True,(0,225,114),2) # 将True改为False图形不会封闭
cv2.polylines(img,[pts],False,(100,12,150),2)

# 添加文字
font=cv2.FONT_HERSHEY_SIMPLEX
cv2.putText(img,'hello',(100,500),font,5,(255,123,123))

cv2.imshow('img',img)
cv2.waitKey(0)
cv2.destroyAllWindows()

```

案例：画OPenCV的logo图

```python
# -*- coding: utf-8 -*-
import math
import cv2
import numpy as np
import matplotlib.pyplot as plt

"""设置基本参数"""
W = 1000  # 窗口的宽
Height = int(1.3 * W)  # 窗口的高
D = int(W * 0.2)
O = (int(0.5 * W), int(W * 0.5))

"""大圆的参数"""
A = (int(0.5 * W), int(0.5 * W - D))
B = (int(0.5 * W - math.sqrt(3) / 2 * D), int(0.5 * W + 0.5 * D))
C = (int(0.5 * W + math.sqrt(3) / 2 * D), int(0.5 * W + 0.5 * D))
R1 = int(math.sqrt(3) * 0.4 * D)

"""小圆参数，圆心同上，半径为"""
R2 = int(math.sqrt(3) * 0.2 * D)

"""扇形参数"""
ang = 60

"""生成一张黑色的图像"""
img = np.zeros((Height, W, 3), np.uint8)

"""画大圆"""
cv2.circle(img, A, R1, (0, 0, 255), -1)
cv2.circle(img, B, R1, (0, 255, 0), -1)
cv2.circle(img, C, R1, (255, 0, 0), -1)

"""画小圆"""
cv2.circle(img, A, R2, (0, 0, 0), -1)
cv2.circle(img, B, R2, (0, 0, 0), -1)
cv2.circle(img, C, R2, (0, 0, 0), -1)

"""方法一：画扇形"""
cv2.ellipse(img, A, (R1, R1), ang, 0, ang, (0, 0, 0), -1)
cv2.ellipse(img, B, (R1, R1), 360 - ang, 0, ang, (0, 0, 0), -1)
cv2.ellipse(img, C, (R1, R1), 360 - 2 * ang, 0, ang, (0, 0, 0), -1)
# 可以给扇形一个颜色，查看所画的扇形
cv2.ellipse(img, A, (R1, R1), ang, 0, ang, (255, 255, 200), -1)
cv2.ellipse(img, B, (R1, R1), 360 - ang, 0, ang, (255, 255, 200), -1)
cv2.ellipse(img, C, (R1, R1), 360 - 2 * ang, 0, ang, (255, 255, 200), -1)

"""写文字"""
font = cv2.FONT_HERSHEY_SIMPLEX
cv2.putText(img, 'OpenCV', (100, 1000), font, 7, (255, 255, 255), 20)

"""运行窗口显示"""
cv2.imwrite("imgs/OpenCV_logo.png", img)
cv2.imshow("OpenCV_log", img)
cv2.waitKey(0)
cv2.destroyAllWindows()

"""matplotlib显示"""
# imageRGB=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
# plt.imshow(imageRGB)
# plt.show()
```

效果图：

![cv3](https://gitee.com/cheng2931/python_cv/raw/master/cvimg/cv3.png)

# 3.鼠标绘图

## 1.鼠标当画笔

主要函数：cv2.setMouseCallback()

**查看支持的鼠标操作**

```python
import cv2

events=[i for i in dir(cv2) if 'EVENT' in i]
print(events)

fonts=[i for i in dir(cv2) if 'FONT' in i]
print(fonts)
```

**双击鼠标左键画圆：**

```python
"""相应鼠标操作的函数"""
def draw_circle(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDBLCLK:
        cv2.circle(img, (x, y), 100, (255, 0, 0), -1)

# 创建黑色图像
img = np.zeros((512, 512, 3), np.uint8)
# 创建一个窗口
cv2.namedWindow('image')
# 函数与窗口绑定
cv2.setMouseCallback('image', draw_circle)
while True:
    cv2.imshow('image', img)
    # 按esc键退出
    if cv2.waitKey(20) & 0XFF == 27:
        break
cv2.destroyAllWindows()
```

**拖动鼠标绘制矩形和曲线**

回调函数包括两部分，一部分画矩形，一部分画曲线。

```python
# -*- coding: utf-8 -*-
import cv2
import numpy as np

drawing = False  # 默认False，鼠标按下是True
mode = True  # if True ，画出矩形，按m切换到曲线
ix, iy = -1, -1

"""响应鼠标的函数"""
def draw_circle(event, x, y, flags, param):
    global ix, iy, drawing, mode

    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix, iy = x, y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing == True:
            if mode == True:
                cv2.rectangle(img, (ix, iy), (x, y), (0, 255, 0), -1)
            else:
                cv2.circle(img, (x, y), 5, (0, 0, 255), -1)
    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        if mode == True:
            cv2.rectangle(img, (ix, iy), (x, y), (0, 255, 0), -1)
        else:
            cv2.circle(img, (x, y), 5, (0, 0, 255), -1)

"""将函数与窗口绑定"""
img = np.zeros((512, 512, 3), np.uint8)
cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_circle)

while True:
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0XFF
    if k == ord('q'):
        mode = not mode
    elif k == 27:
        break
cv2.destroyAllWindows()
```

p17



























